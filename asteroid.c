#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <time.h>
#include <sys/time.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h> // symbols with the prefix XA_

#include "asteroid.h"

void wrapCoordinates(View *view, float ix, float iy, float *ox, float *oy)
{
    *ox = ix;
    *oy = iy;

    if (ix < 0.0f)
    {
        *ox = ix + (float) view->width;
    }
    if (ix >= (float) view->width)
    {
        *ox = ix - (float) view->width;
    }
    if (iy < 0.0f)
    {
        *oy = iy + (float) view->height;
    }
    if (iy >= (float) view->height)
    {
        *oy = iy - (float) view->height;
    }
}

void drawPixel(View *view, float x, float y, unsigned int color)
{
    wrapCoordinates(view, x, y, &x, &y);
    char *row = view->buffer + ((int)y * view->pitch);
    unsigned int *p = (unsigned int *) (row + ((int)x * view->pixelBytes));
    *p = color;
}

/*
 * Bresenham's line algorithm
 * Given points (x0,y0) and (x1,y2), dx=x2-x1, dy=y2-y1.  The driving axis
 * (da) is the x-axis if |dx| >= |dy|, and the y-axis if |dy| > |dx|. The da is
 * used as the "axis of control" for the algorithm and is the axis of maximum
 * movement. Within the main loop of the algorithm, the coordinate
 * corresponding to the da is incremented by one unit. Visually, the error
 * represents from the diagonal point until the top of the square. If it is
 * within the square, then it only increments the coordinate cooresponding to
 * da else if it is above the square (into a new square), it increments both
 * coordinates.
 */
void drawLine(View *view, int x0, int y0, int x1, int y1)
{
    int dx = abs(x1-x0);
    int dy = abs(y1-y0);
    int sx = x0 < x1 ? 1 : -1;
    int sy = y0 < y1 ? 1 : -1;
    int da = dx > dy ? dx : -dy;
    int err0 = da / 2;
    int err1 = err0;

    for (;;)
    {
        drawPixel(view, x0, y0, 0xfffffff);
        if (x0 == x1 && y0 == y1)
        {
            break;
        }
        err1 = err0;
        if (err1 > -dx)
        {
            err0 -= dy;
            x0 += sx;
        }
        if (err1 < dy)
        {
            err0 += dx;
            y0 += sy;
        }
    }
}

void drawWireFrameModel(View *view, Model *model,
        float x,
        float y,
        float r,
        float scaleFactor)
{
    float transformX[model->length];
    float transformY[model->length];
    int len = model->length;

    // Rotate
    for (int i = 0; i < len; i++)
    {
        transformX[i] = model->x[i] * cosf(r) - model->y[i] * sinf(r);
        transformY[i] = model->x[i] * sinf(r) + model->y[i] * cosf(r);
    }

    // Scale
    for (int i = 0; i < len; i++)
    {
        transformX[i] = transformX[i] * scaleFactor;
        transformY[i] = transformY[i] * scaleFactor;
    }

    // Translate - offset from where the object is
    for (int i = 0; i < len; i++)
    {
        transformX[i] = transformX[i] + x;
        transformY[i] = transformY[i] + y;
    }

    for (int i = 0; i < len; i++)
    {
        int j = i + 1;
        drawLine(view, transformX[i%len],transformY[i%len],
                transformX[j%len],transformY[j%len]);
    }
}

int isPointInsideCircle(float cx, float cy, float radius, float x, float y)
{
    return sqrt((x-cx)*(x-cx) + (y-cy)*(y-cy)) < radius;
}

int randRange(int min, int max)
{
    return rand() % (max + 1 - min) + min;
}

Asteroid *newAsteroid(float x, float y, float dx, float dy, int size, float angle)
{
    Asteroid *a = malloc(sizeof(Asteroid));
    a->x = x;
    a->y = y;
    a->dx = dx;
    a->dy = dy;
    a->size = size;
    a->angle = angle;
    return a;
}

int main(void)
{
    View view;

    view.width = WIDTH;
    view.height = HEIGHT;
    view.pixelBits = 32;
    view.pixelBytes = view.pixelBits / 8;

    // Display structure that serves as the connection to the X server and that
    // contains all the information about that X server
    Display *display = XOpenDisplay(NULL /* display_name */); // connects to X server
    if (display == NULL) return 1;

    int root = DefaultRootWindow(display);
    int defaultScreen = DefaultScreen(display);

    XVisualInfo visInfo = {};
    Status status = XMatchVisualInfo(
                display,          // connection to X server
                defaultScreen,    // screen
                24,               // depth of the screen
                TrueColor,        // class of the screen
                &visInfo);        // match visual info
    if (status == 0) return 1;

    XSetWindowAttributes windowAttr;
    windowAttr.background_pixel = 0;
    windowAttr.colormap = XCreateColormap(
            display,        // connection to X server
            root,           // window
            visInfo.visual, // visual type
            AllocNone);     // colormap entries to be allocated
    windowAttr.event_mask = StructureNotifyMask | KeyPressMask | KeyReleaseMask;
    windowAttr.bit_gravity = StaticGravity;

    unsigned long attrMask = CWBackPixel|CWColormap|CWEventMask|CWBitGravity;

    // create an unmapped window (not yet shown)
    Window window = XCreateWindow(
            display,                            // connection to X server
            root,                               // window
            0,                                  // x
            0,                                  // y
            view.width,                         // width
            view.height,                        // height
            0,                                  // border width
            visInfo.depth,                      // depth
            InputOutput,                        // window's class
            visInfo.visual,                     // visual type
            attrMask,                           // value mask
            &windowAttr                         // attributes
            );
    if (!window) return 1;

    int bestScore = 0;
    int currentScore = 0;
    int deaths = 0;
    char windowName[256];
    sprintf(windowName, "Asteroid ms:%f best:%d score:%d deaths:%d", 0.0f,
            bestScore, currentScore, deaths);
    XStoreName(display, window, windowName);
    XMapWindow(display, window); // map to window to become viewable

    XFlush(display); // flush the output buffer

    Atom WM_DELETE_WINDOW = XInternAtom(display, "WM_DELETE_WINDOW", False);
    if (!XSetWMProtocols(display, window, &WM_DELETE_WINDOW, 1))
    {
        fprintf(stderr, "Couldn't register WM_DELETE_WINDOW properly\n");
    }

    view.windowBufferSize = view.width * view.height * view.pixelBytes;
    view.pitch = view.width * view.pixelBytes;
    view.buffer = (char *) malloc(view.windowBufferSize);

    XImage *xWindowBuffer = XCreateImage(
            display,
            visInfo.visual,
            visInfo.depth,
            ZPixmap,   // format of the image
            0,         // offset
            view.buffer,    // image data
            view.width,
            view.height,
            view.pixelBits, // scanline bits
            0          // offset bytes per line
            );

    GC defaultGC = DefaultGC(display, defaultScreen); // graphics context

    srand(time(NULL));

    // init spaceship
    Spaceship player = {view.width / 2.0f, view.height / 2.0f, 0.0f, 0.0f, 0.0f, 1};

    Bullet *bullets[NUM_BULLETS];
    memset(bullets, 0, sizeof(Bullet*) * NUM_BULLETS);
    int bulletIndex = 0;

    // init asteroids
    Asteroid *asteroids[NUM_ASTEROIDS];
    memset(asteroids, 0, sizeof(Asteroid*) * NUM_ASTEROIDS);
    int asteroidIndex = 0;
    for (; asteroidIndex < NUM_ASTEROIDS/8; asteroidIndex++)
    {
        float x = (float) randRange(0, view.width);
        float y = (float) randRange(0, view.height);
        float dx = (float) randRange(-40, 40);
        float dy = (float) randRange(-40, 40);
        int size = (float) randRange(4, 32);
        asteroids[asteroidIndex] = newAsteroid(x, y, dx, dy, size, 0);
    }

    Model *asteroidModels[NUM_ASTEROIDS];
    for (int i = 0; i < NUM_ASTEROIDS; i++)
    {
        Model *m = malloc(sizeof(Model));
        m->x = calloc(NUM_VERTICES, sizeof(float));
        m->y = calloc(NUM_VERTICES, sizeof(float));
        m->length = NUM_VERTICES;
        for (int i = 0; i < NUM_VERTICES; i++)
        {
            float radius = (float)rand() / (float)RAND_MAX * 0.4f + 0.8f;
            float angle = ((float)i / (float)NUM_VERTICES) * TAU; // degree per segment
            m->x[i] = radius * sinf(angle);
            m->y[i] = radius * cosf(angle);
        }
        asteroidModels[i] = m;
    }

    //
    // start game loop
    //

    char isLeftPressed = 0;
    char isRightPressed = 0;
    char isUpPressed = 0;
    char isXPos = 0;
    char isYPos = 0;

    struct timeval t1, t2;
    gettimeofday(&t1, NULL);

    int isWindowOpen = 1;
    while (isWindowOpen)
    {
        float dt = 0;

        gettimeofday(&t2, NULL);

        dt = (t2.tv_sec - t1.tv_sec);
        dt += (t2.tv_usec - t1.tv_usec) * 0.000001;
        t1 = t2;

        XEvent ev = {};

        while (XPending(display) > 0)
        {
            XNextEvent(display, &ev);
            switch (ev.type)
            {
                case DestroyNotify:
                {
                    XDestroyWindowEvent *e = (XDestroyWindowEvent *) &ev;
                    if (e->window == window)
                    {
                        isWindowOpen = 0;
                    }
                } break;
                case ClientMessage:
                {
                    XClientMessageEvent *e = (XClientMessageEvent *)&ev;
                    if ((Atom)e->data.l[0] == WM_DELETE_WINDOW)
                    {
                        XDestroyWindow(display, window);
                        isWindowOpen = 0;
                    }
                } break;
                case ConfigureNotify:
                {
                    XConfigureEvent *e = (XConfigureEvent *) &ev;
                    view.width = e->width;
                    view.height = e->height;

                    XDestroyImage(xWindowBuffer);

                    view.windowBufferSize = view.width * view.height * view.pixelBytes;
                    view.pitch = view.width * view.pixelBytes;
                    view.buffer = (char *) malloc(view.windowBufferSize);

                    player.x = view.width / 2.0f;
                    player.y = view.height / 2.0f;

                    xWindowBuffer = XCreateImage(display, visInfo.visual,
                            visInfo.depth, ZPixmap, 0, view.buffer, view.width,
                            view.height, view.pixelBits, 0);
                } break;
                case KeyPress:
                {
                    XKeyPressedEvent *e = (XKeyPressedEvent*) &ev;

                    if (e->keycode == XKeysymToKeycode(display, XK_Left))
                    {
                        isLeftPressed = 1;
                    }
                    if (e->keycode == XKeysymToKeycode(display, XK_Right))
                    {
                        isRightPressed = 1;
                    }
                    if (e->keycode == XKeysymToKeycode(display, XK_Up))
                    {
                        isUpPressed = 1;
                    }
                    if (e->keycode == SPACE_KEY)
                    {
                        Bullet *bullet = calloc(1, sizeof(Bullet));
                        bullet->x = player.x;
                        bullet->y = player.y;
                        bullet->dx = BULLET_SPEED * sinf(player.angle);
                        bullet->dy = -BULLET_SPEED * cosf(player.angle);

                        while (bullets[bulletIndex])
                        {
                            bulletIndex++;
                        }

                        bullets[bulletIndex++] = bullet;
                        bulletIndex = bulletIndex % NUM_BULLETS;
                    }
                } break;
                case KeyRelease:
                {
                    XKeyPressedEvent *e = (XKeyPressedEvent*) &ev;

                    if (e->keycode == XKeysymToKeycode(display, XK_Left))
                    {
                        isLeftPressed = 0;
                    }
                    if (e->keycode == XKeysymToKeycode(display, XK_Right))
                    {
                        isRightPressed = 0;
                    }
                    if (e->keycode == XKeysymToKeycode(display, XK_Up))
                    {
                        isUpPressed = 0;
                        isXPos = player.dx > 0;
                        isYPos = player.dx > 0;
                    }
                } break;
            }
        }

        memset(view.buffer, 0, view.windowBufferSize);

        if (isLeftPressed)
        {
            player.angle -= ANGLE_SPEED * dt;
        }
        if (isRightPressed)
        {
            player.angle += ANGLE_SPEED * dt;
        }
        if (isUpPressed)
        {
            player.dx += sin(player.angle) * THRUST * dt;
            player.dy += -cos(player.angle) * THRUST * dt;
        }
        else
        {
            player.dx *= FRICTION;
            player.dy *= FRICTION;
        }

        player.x += player.dx * dt;
        player.y += player.dy * dt;

        wrapCoordinates(&view, player.x, player.y, &player.x, &player.y);

        // isosceles triangle
        float shipModelX[3] = { 0.0f, -2.5f, 2.5f };
        float shipModelY[3] = { -5.5f, 2.5f, 2.5f };
        Model shipModel = {shipModelX, shipModelY, 3};

        if (player.isAlive)
        {
            drawWireFrameModel(&view, &shipModel, player.x, player.y,
                    player.angle, 1.5);
        }
        else
        {
            currentScore = 0;
            player.isAlive = 1;
            player.x = view.width / 2.0f;
            player.y = view.height / 2.0f;
            player.dx = player.dy = player.angle = 0.0f;

            for (int i = 0; i < NUM_ASTEROIDS; i++)
            {
                Asteroid *asteroid = asteroids[i];
                if (asteroid)
                {
                    free(asteroid);
                    asteroids[i] = NULL;
                }
            }

            asteroidIndex = 0;
            for (; asteroidIndex < NUM_ASTEROIDS/8; asteroidIndex++)
            {
                float x = (float) randRange(0, view.width);
                float y = (float) randRange(0, view.height);
                float dx = (float) randRange(-40, 40);
                float dy = (float) randRange(-40, 40);
                int size = (float) randRange(4, 32);
                asteroids[asteroidIndex] = newAsteroid(x, y, dx, dy, size, 0);
            }

            for (int i = 0; i < NUM_BULLETS; i++)
            {
                Bullet *bullet = bullets[i];
                if (bullet)
                {
                    free(bullet);
                    bullets[i] = NULL;
                }
            }
        }

        for (int i = 0; i < NUM_ASTEROIDS; i++)
        {
            Asteroid *asteroid = asteroids[i];
            if (asteroid)
            {
                asteroid->x += asteroid->dx * dt;
                asteroid->y += asteroid->dy * dt;
                asteroid->angle += 0.5f * dt;

                wrapCoordinates(&view, asteroid->x, asteroid->y, &asteroid->x,
                        &asteroid->y);

                drawWireFrameModel(&view, asteroidModels[i], asteroid->x, asteroid->y,
                        asteroid->angle, asteroid->size);

                Asteroid *a = asteroid;
                if (isPointInsideCircle(a->x, a->y, a->size, player.x, player.y))
                {
                    player.isAlive = 0;
                    deaths++;
                }
            }
        }

        for (int i = 0; i < NUM_BULLETS; i++)
        {
            Bullet *bullet = bullets[i];
            if (bullet)
            {
                if (bullet->x < 0 || bullet->y < 0 || bullet->x >= view.width
                        || bullet->y >= view.height)
                {
                    free(bullet);
                    bullets[i] = NULL;
                    bullet = NULL;
                }
                else
                {
                    bullet->x += bullet->dx * dt;
                    bullet->y += bullet->dy * dt;

                    drawPixel(&view, bullet->x, bullet->y, 0xfffffff);
                }
            }

            for (int j = 0; j < NUM_ASTEROIDS; j++)
            {
                Asteroid *a = asteroids[j];
                Bullet *b = bullet;
                if (a && b && isPointInsideCircle(a->x, a->y, a->size, b->x, b->y))
                {
                    free(bullet);
                    bullets[i] = NULL;
                    bullet = NULL;
                    currentScore += 100;
                    if (currentScore > bestScore)
                    {
                        bestScore = currentScore;
                    }

                    if (a->size > 4)
                    {
                        float randAngle1 = ((float)rand() / (float)RAND_MAX) * TAU;
                        float randAngle2 = ((float)rand() / (float)RAND_MAX) * TAU;
                        float splitSpeed = (float) randRange(10, 50);

                        wrapCoordinates(&view, a->x, a->y, &a->x, &a->y);

                        asteroids[asteroidIndex++] = newAsteroid(a->x, a->y,
                                splitSpeed * sinf(randAngle1), splitSpeed *
                                cosf(randAngle1), (int) (a->size >> 1), 0);

                        asteroidIndex = asteroidIndex % NUM_ASTEROIDS;
                        if (asteroids[asteroidIndex])
                        {
                            free(asteroids[asteroidIndex]);
                            asteroids[asteroidIndex] = NULL;
                        }

                        asteroids[asteroidIndex++] = newAsteroid(a->x, a->y,
                                10.0f * sinf(randAngle2), 10.0f *
                                cosf(randAngle2), (int) (a->size >> 1),
                                0);

                        asteroidIndex = asteroidIndex % NUM_ASTEROIDS;
                        if (asteroids[asteroidIndex])
                        {
                            free(asteroids[asteroidIndex]);
                            asteroids[asteroidIndex] = NULL;
                        }
                    }

                    free(a);
                    asteroids[j] = NULL;
                }
            }
        }

        XPutImage(display, window, defaultGC, xWindowBuffer, 0, 0, 0, 0, view.width,
                view.height);

        sprintf(windowName, "Asteroid ms:%f best:%d score:%d deaths:%d", dt * 1000.0f,
                bestScore, currentScore, deaths);
        XStoreName(display, window, windowName);
    }

    // clean up
    for (int i = 0; i < NUM_ASTEROIDS; i++)
    {
        Asteroid *asteroid = asteroids[i];
        if (asteroid)
        {
            free(asteroid);
        }
    }

    for (int i = 0; i < NUM_BULLETS; i++)
    {
        Bullet *bullet = bullets[i];
        if (bullet)
        {
            free(bullet);
        }
    }

    for (int i = 0; i < NUM_ASTEROIDS; i++)
    {
        Model *m = asteroidModels[i];
        free(m->x);
        free(m->y);
        free(m);
    }

    return 0;
}
