#ifndef _asteroid_h
#define _asteroid_h

typedef struct
{
    float x;
    float y;
    float dx;
    float dy;
    float angle;
    char isAlive;
} Spaceship;

typedef struct
{
    float x;
    float y;
    float dx;
    float dy;
    int size;
    float angle;
} Asteroid;

typedef struct
{
    float x;
    float y;
    float dx;
    float dy;
} Bullet;

typedef struct
{
    int width;
    int height;
    int pixelBits;
    int pixelBytes;
    int windowBufferSize;
    char *buffer;
    int pitch;
} View;

typedef struct
{
    float *x;
    float *y;
    int length;
} Model;

#define WIDTH 800
#define HEIGHT 600

#define NUM_ASTEROIDS 256
#define NUM_BULLETS 256
#define NUM_VERTICES 20

#define BULLET_SPEED 200.0f
#define ANGLE_SPEED 8.0f
#define THRUST 250.0f
#define FRICTION 0.9995f

#define TAU 6.28318f

#define SPACE_KEY 0x41

#endif // _asteroid_h

